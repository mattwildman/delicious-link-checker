<?php

$apiUrl = 'https://'.$argv[1].':'.$argv[2].
	      '@api.del.icio.us/v1/posts/';
$userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1897.2 Safari/537.36";

class Post {
	var $title;
	var $href;
	var $description;
	var $tags = array();
	
	function __construct($title, $href, $description, $tagString) {
		$this->title = $title;
		$this->href = $href;
		$this->description = $description;
		$this->tags = preg_split("/\s/", $tagString);
	}
}

//get posts
function get_posts($tag, $maxResults=100000) {
	echo "Getting links...".PHP_EOL;
	global $apiUrl;
	
	$url = $apiUrl.'all?tag='.$tag.'&results='.$maxResults;
	$posts = array();
	
	$x = new XMLReader();
	
	try {
		$x->open($url);
		$x->read(); //move past 'posts' root node
		echo "Parsing XML...".PHP_EOL;
		while ($x->read()) { //for each 'post' node
			$node = $x->expand();
			$title = $node->getAttribute("description");
			$href = $node->getAttribute("href");
			$description = $node->getAttribute("extended");
			$tagString = $node->getAttribute("tag");
			$post = new Post($title, $href, $description, $tagString);
			$posts[] = $post;
		}
		$x->close();
		array_pop($posts); //remove final blank XML node read
	}
	catch(Exception $e) {}
	
	return $posts;
}

//re-add and replace with no linkError tags and public
function update_posts($posts) {
	global $apiUrl, $userAgent;
	echo "Start update (updating ".count($posts)." posts)".PHP_EOL;
	foreach ($posts as $k => $post) {
		$k++;
		echo "Updating ".$post->title." on Delicious [".$k."/".count($posts)."]".PHP_EOL;
		$tagString = "";
		foreach ($post->tags as $tag) {
			if ($tag != "linkError" && $tag != "redirectError" && $tag != "unknownError")
				$tagString .= ",".$tag;
		}
		$url = $apiUrl.'add?url='.urlencode($post->href).
				  '&description='.urlencode($post->title).
					 '&extended='.urlencode($post->description).
						 '&tags='.substr($tagString, 1).
					  '&replace=yes'.
					   '&shared=yes';
		$ch = curl_init($url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, false); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$r = curl_exec($ch);
		if(!$r){
			echo "LINK UPDATE FAILED: \"".curl_error($ch)."\" - Code: ".curl_errno($ch).PHP_EOL;
			$updateErrors[] = $post;
		}
		else {
			echo PHP_EOL;
		}
		curl_close($ch);
	}
}

function run() {
	update_posts(get_posts('linkError'));
}

run();

?>