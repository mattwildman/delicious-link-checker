<?php

$apiUrl = 'https://'.$argv[1].':'.$argv[2].
	      '@api.del.icio.us/v1/posts/';
$userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1897.2 Safari/537.36";

$posts = array();
$okLinks = 0;

$clientErrors = array();
$redirects = array();
$serverErrors = array();
$unknownErrors = array();

$updateErrors = array();

$badUrls = array();
$emptyTitles = array();
$longUrls = array();
$longTitles = array();

$maxTitleLength = 80;
$maxUrlLength = 100;

class Post {
	var $title;
	var $href;
	var $description;
	var $status = "OK";
	var $statusCode = "200";
	var $tags = array();
	var $hasErrors = false;
	
	function __construct($title, $href, $description, $tagString) {
		$this->title = $title;
		$this->href = $href;
		$this->description = $description;
		$this->tags = preg_split("/\s/", $tagString);
	}
}

function get_links() {
	echo "Getting links...".PHP_EOL;
	global $apiUrl, $posts;
	
	$maxResults = 7000; //maximum 100000
	
	$url = $apiUrl.'all?results='.$maxResults;
	
	$x = new XMLReader();
	
	try {
		$x->open($url);
		$x->read(); //move past 'posts' root node
		echo "Parsing XML...".PHP_EOL;
		while ($x->read()) { //for each 'post' node
			$node = $x->expand();
			$title = $node->getAttribute("description");
			$href = $node->getAttribute("href");
			$description = $node->getAttribute("extended");
			$tagString = $node->getAttribute("tag");
			$post = new Post($title, $href, $description, $tagString);
			$posts[] = $post;
		}
		$x->close();
		array_pop($posts); //remove final blank XML node read
		return true;
	}
	catch(Exception $e) {
		return false;
	}
}

function update_post($post) {
	global $apiUrl, $userAgent;
	echo "Updating post on Delicious...".PHP_EOL;
	$tagString = "";
	foreach ($post->tags as $tag) {
		$tagString .= ",".$tag;
	}
	$url = $apiUrl.'add?url='.urlencode($post->href).
	          '&description='.urlencode($post->title).
			     '&extended='.urlencode($post->description).
	                 '&tags='.substr($tagString, 1).
				  '&replace=yes'.
				   '&shared=no';
	$ch = curl_init($url); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, false); 
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$r = curl_exec($ch);
	if(!$r){
		echo "LINK UPDATE FAILED: \"".curl_error($ch)."\" - Code: ".curl_errno($ch).PHP_EOL;
		$updateErrors[] = $post;
	}
	else {
		echo PHP_EOL;
	}
	curl_close($ch);
}

function get_status_code($url) {
	global $userAgent;
	$ch = curl_init($url); 
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
	
	$r = curl_exec($ch); 
	$c = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	
	return $c;
}

function check_links() {
	global $posts, $okLinks, $clientErrors, $redirects, $serverErrors, $unknownErrors, $updateErrors,
	       $badUrls, $emptyTitles, $longUrls, $longTitles, $maxTitleLength, $maxUrlLength;
	
	$i = 1;
	$t = sizeof($posts);
		   
	echo "Checking ".$t." links...".PHP_EOL;
	
	foreach ($posts as $post) {	
		echo "Processing link ".$i++."/".$t.": ".$post->href." ";
    	if (strlen($post->href) > $maxUrlLength) {
			$post->tags[] = "longUrl";
			$post->hasErrors = true;
			$longUrls[] = $post;
		}
		if (strlen($post->title) > $maxTitleLength) {
			$post->tags[] = "longTitle";
			$post->hasErrors = true;
			$longTitles[] = $post;
		}
		if (strlen($post->title) == "") {
			$post->tags[] = "noTitle";
			$post->title = $post->href;
			$post->hasErrors = true;
			$emptyTitles[] = $post;
		}
    	if (!filter_var($post->href, FILTER_VALIDATE_URL) === FALSE) {
			try {
				$post->statusCode = get_status_code($post->href);
			}
			catch(Exception $e) {
				$post->statusCode = "N/A";
			}
			if ($post->statusCode == "200") {
				$post->status = "OK";
				$okLinks++;
			}
			else {
				if ($post->statusCode == "404") {
					$post->status = "Not found";
					$post->tags[] = "notFound";
					$clientErrors[] = $post;
				}
				else {
					$statusIndex = substr($post->statusCode, 0, 1);
					switch ($statusIndex) {
						case "1":
							$post->status = "Unknown";
							$post->tags[] = "unknownError";
							$unknownErrors[] = $post;
							break;
						case "2":
							$post->status = "Unknown";
							$post->tags[] = "unknownError";
							$unknownErrors[] = $post;
							break;
						case "3":
							$post->status = "Redirected";
							$post->tags[] = "redirectError";
							$redirects[] = $post;
							break;
						case "4":
							$post->status = "Link error";
							$post->tags[] = "unknownError";
							$unknownErrors[] = $post;
							break;
						case "5":
							$post->status = "Server error";
							$post->tags[] = "serverError";
							$serverErrors[] = $post;
							break;
						default:
							$post->status = "Unknown";
							if ($post->statusCode == "") {
								$post->statusCode = "???";
							}
							$post->tags[] = "unknownError";
							$unknownErrors[] = $post;
							break;
					}
				}
				$post->hasErrors = true;
			}
    	}
		else {
			$post->statusCode = "N/A";
			$post->status = "Bad URL";
			$post->tags[] = "badUrl";
			$post->hasErrors = true;
			$badUrls[] = $post;
		}
		echo "(".$post->statusCode.")".PHP_EOL;
		if ($post->hasErrors ) {
			$post->tags[] = "linkError";
			update_post($post);
		}
    }
	
}

function process_links() {
	
	global $posts, $okLinks, $clientErrors, $redirects, $serverErrors, $unknownErrors,
	       $updateErrors, $badUrls, $emptyTitles, $longUrls, $longTitles;
	
	if (get_links()) {
		
		check_links();

		echo "Number of links: ".sizeof($posts).PHP_EOL.
			 "OK links: ".$okLinks.PHP_EOL.
			 "Link errors: ".sizeof($clientErrors).PHP_EOL.
			 "Server errors: ".sizeof($serverErrors).PHP_EOL.
			 "Redirects: ".sizeof($redirects).PHP_EOL.
			 "Unknown errors: ".sizeof($unknownErrors).PHP_EOL.
			 "Long titles: ".sizeof($longTitles).PHP_EOL.
			 "Long URLs: ".sizeof($longUrls).PHP_EOL;
	}
	else {
		echo "There was a problem connecting to your Delicious account.".PHP_EOL;
	}
	
}

set_error_handler(
	function($severity, $message, $file, $line) { 
		throw new ErrorException($message, $severity, $severity, $file, $line); 
	}
);
set_time_limit(20000);
process_links();

?>