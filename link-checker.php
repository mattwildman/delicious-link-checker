<html>
    <head>
        <title>Your links</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <h1>Your links</h1>

<?php

$posts = array();
$okLinks = 0;

$clientErrors = array();
$redirects = array();
$serverErrors = array();
$unknownErrors = array();

$badUrls = array();
$emptyTitles = array();
$longUrls = array();
$longTitles = array();

$indent = "        ";
$maxTitleLength = 80;
$maxUrlLength = 100;

class Post {

	var $title;
	var $href;
	var $status = "OK";
	var $statusCode = "200";
	
	function __construct($title, $href) {
		$this->title = $title;
		$this->href = $href;
	}

}

function get_links($username, $password) {
	
	global $posts;
	
	$maxResults = 7000; //maximum 100000
	
	$url = 'https://'.$username.':'.$password.
	       '@api.del.icio.us/v1/posts/all?results='.$maxResults;
	
	$x = new XMLReader();
	
	try {
		$x->open($url);
		$x->read(); //move past 'posts' root node	
		while ($x->read()) { //for each 'post' node
			$node = $x->expand();
			$title = $node->getAttribute("description");
			$href = $node->getAttribute("href");
			$post = new Post($title, $href);
			$posts[] = $post;
		}
		$x->close();
		array_pop($posts); //remove final blank XML node read
		return true;
	}
	catch(Exception $e) {
		return false;
	}
}

function get_status_code($url) {
	$ch = curl_init($url); 
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch,CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1897.2 Safari/537.36');
	
	$r = curl_exec($ch); 
	$c = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	return $c;
}

function check_links() {
	
	global $posts, $okLinks, $clientErrors, $redirects, $serverErrors, $unknownErrors, 
	       $badUrls, $emptyTitles, $longUrls, $longTitles, $maxTitleLength, $maxUrlLength;
	
	foreach ($posts as $post) {
    	if (strlen($post->href) > $maxUrlLength) {
			$longUrls[] = $post;
		}
		if (strlen($post->title) > $maxTitleLength) {
			$longTitles[] = $post;
		}
		if (strlen($post->title) == "") {
			$post->title = $post->href;
			$emptyTitles[] = $post;
		}
    	if (!filter_var($post->href, FILTER_VALIDATE_URL) === FALSE) {
			try {
				$post->statusCode = get_status_code($post->href); //substr(get_status_code($post->href)[0], 9, 3);
			}
			catch(Exception $e) {
				$post->statusCode = "N/A";
			}
			if ($post->statusCode == "200") {
				$post->status = "OK";
				$okLinks++;
			}
			else if ($post->statusCode == "404") {
				$post->status = "Not found";
				$clientErrors[] = $post;
			}
			else {
				$statusIndex = substr($post->statusCode, 0, 1);
				switch ($statusIndex) {
					case "1":
						$post->status = "Unknown";
						$unknownErrors[] = $post;
						break;
					case "2":
						$post->status = "Unknown";
						$unknownErrors[] = $post;
						break;
					case "3":
						$post->status = "Redirected";
						$redirects[] = $post;
						break;
					case "4":
						$post->status = "Link error";
						$unknownErrors[] = $post;
						break;
					case "5":
						$post->status = "Server error";
						$serverErrors[] = $post;
						break;
					default:
						$post->status = "Unknown";
						if ($post->statusCode == "") {
							$post->statusCode = "???";
						}
						$unknownErrors[] = $post;
						break;
				}
			}
    	}
		else {
			$post->statusCode = "N/A";
			$post->status = "Bad URL";
			$badUrls[] = $post;
		}
    }
	
}

function print_error_table($posts) {
	
	global $indent;
	
	echo $indent."<table>".PHP_EOL.
	     $indent."    <tr>".PHP_EOL.
	     $indent."       <th>Link</th>".PHP_EOL.
	     $indent."       <th>Status code</th>".PHP_EOL.
	     $indent."    </tr>".PHP_EOL;
	
	foreach ($posts as $post) {
		echo	$indent."    <tr>".PHP_EOL.
				$indent."       <td><a href=\"".$post->href."\">".$post->title."</a></td>".PHP_EOL.
				$indent."       <td>".$post->statusCode."</td>".PHP_EOL.
				$indent."    </tr>".PHP_EOL;
	}
	
	echo $indent."</table>".PHP_EOL;
}

function process_links($username, $password) {
	
	global $posts, $okLinks, $clientErrors, $redirects, $serverErrors, $unknownErrors,
	       $badUrls, $emptyTitles, $longUrls, $longTitles, $indent;
	
	if (get_links($username, $password)) {
		
		check_links();

		echo $indent."<p>Number of links: ".sizeof($posts)."</p>".PHP_EOL.
			 $indent."<p>OK links: ".$okLinks."</p>".PHP_EOL.
			 $indent."<p>Link errors: ".sizeof($clientErrors)."</p>".PHP_EOL.
			 $indent."<p>Server errors: ".sizeof($serverErrors)."</p>".PHP_EOL.
			 $indent."<p>Redirects: ".sizeof($redirects)."</p>".PHP_EOL.
			 $indent."<p>Unknown errors: ".sizeof($unknownErrors)."</p>".PHP_EOL.
			 $indent."<p>Long titles: ".sizeof($longTitles)."</p>".PHP_EOL.
			 $indent."<p>Long URLs: ".sizeof($longUrls)."</p>".PHP_EOL;

		if (sizeof($clientErrors) > 0) {
			echo $indent."<h2>Link errors</h2>".PHP_EOL.
				 $indent."<p>Explanation of link errors and how to fix them.</p>".PHP_EOL;
			print_error_table($clientErrors);
		}

		if (sizeof($serverErrors) > 0) {
			echo $indent."<h2>Server errors</h2>".PHP_EOL.
				 $indent."<p>Explanation of server errors and how to fix them.</p>".PHP_EOL;
			print_error_table($serverErrors);
		}

		if (sizeof($redirects) > 0) {
			echo $indent."<h2>Redirected links</h2>".PHP_EOL.
				 $indent."<p>Explanation of redirects and how to fix them.</p>".PHP_EOL;
			print_error_table($redirects);
		}

		if (sizeof($unknownErrors) > 0) {
			echo $indent."<h2>Unknown errors</h2>".PHP_EOL.
				 $indent."<p>Explanation of unknown errors and how to fix them.</p>".PHP_EOL;
			print_error_table($unknownErrors);
		}
		
		if (sizeof($badUrls) > 0) {
			echo $indent."<h2>Bad URLs</h2>".PHP_EOL.
				 $indent."<p>Explanation of bad URLs and how to fix them.</p>".PHP_EOL.
				 $indent."<table>".PHP_EOL.
				 $indent."    <tr>".PHP_EOL.
				 $indent."       <th>Link</th>".PHP_EOL.
				 $indent."       <th>URL</th>".PHP_EOL.
				 $indent."    </tr>".PHP_EOL;
			foreach ($badUrls as $post) {
				echo	$indent."    <tr>".PHP_EOL.
						$indent."       <td><a href=\"".$post->href."\">".$post->title."</a></td>".PHP_EOL.
						$indent."       <td>".$post->href."</td>".PHP_EOL.
						$indent."    </tr>".PHP_EOL;
			}
			echo $indent."</table>".PHP_EOL;
		}
		
		if (sizeof($emptyTitles) > 0) {
			echo $indent."<h2>No title</h2>".PHP_EOL.
				 $indent."<p>Explanation of links with no title and how to fix them.</p>".PHP_EOL.
				 $indent."<table>".PHP_EOL.
				 $indent."    <tr>".PHP_EOL.
				 $indent."       <th>Link</th>".PHP_EOL.
				 $indent."    </tr>".PHP_EOL;
			foreach ($emptyTitles as $post) {
				echo	$indent."    <tr>".PHP_EOL.
						$indent."       <td><a href=\"".$post->href."\">".$post->title."</a></td>".PHP_EOL.
						$indent."    </tr>".PHP_EOL;
			}
			echo $indent."</table>".PHP_EOL;
		}
		
		if (sizeof($longTitles) > 0) {
			echo $indent."<h2>Long titles</h2>".PHP_EOL.
				 $indent."<p>Explanation of long titles and how to fix them.</p>".PHP_EOL.
				 $indent."<table>".PHP_EOL.
				 $indent."    <tr>".PHP_EOL.
				 $indent."       <th>Link</th>".PHP_EOL.
				 $indent."       <th>Length</th>".PHP_EOL.
				 $indent."    </tr>".PHP_EOL;
			foreach ($longTitles as $post) {
				echo	$indent."    <tr>".PHP_EOL.
						$indent."       <td><a href=\"".$post->href."\">".$post->title."</a></td>".PHP_EOL.
						$indent."       <td>".strlen($post->title)."</td>".PHP_EOL.
						$indent."    </tr>".PHP_EOL;
			}
			echo $indent."</table>".PHP_EOL;
		}

		if (sizeof($longUrls) > 0) {
			echo $indent."<h2>Long Urls</h2>".PHP_EOL.
				 $indent."<p>Explanation of long Urls and how to fix them.</p>".PHP_EOL.
				 $indent."<table>".PHP_EOL.
				 $indent."    <tr>".PHP_EOL.
				 $indent."       <th>Title</th>".PHP_EOL.
				 $indent."       <th>URL</th>".PHP_EOL.
				 $indent."       <th>Length</th>".PHP_EOL.
				 $indent."    </tr>".PHP_EOL;
			foreach ($longUrls as $post) {
				echo	$indent."    <tr>".PHP_EOL.
						$indent."       <td>".$post->title."</td>".PHP_EOL.						
						$indent."       <td><a href=\"".$post->href."\">".$post->href."</a></td>".PHP_EOL.
						$indent."       <td>".strlen($post->href)."</td>".PHP_EOL.
						$indent."    </tr>".PHP_EOL;
			}
			echo $indent."</table>".PHP_EOL;
		}
	}
	else {
		echo $indent."<p>There was a problem connecting to your Delicious account.".
		             "Please try and <a href=\"index.html\" title=\"Log in\">log in</a> again.</p>";
	}
	
}

set_error_handler(
	function($severity, $message, $file, $line) { 
		throw new ErrorException($message, $severity, $severity, $file, $line); 
	}
);
set_time_limit(20000);
process_links($_POST["username"],$_POST["password"]);

?>
    </body>
</html>